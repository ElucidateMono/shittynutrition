import sys
import json
import os

class Plan:
    def __init__(self, name):
        self.name = name
        self.items = []

        self.ri_data = {
            "ri_calories":1500,
            "ri_fat":70,
            "ri_saturated_fat":20,
            "ri_sugar":50,
            "ri_salt":6,
            "ri_fibre":30,
            "ri_protein":90,
            "ri_carbohydrates":180
        } 
    
    def add_item(self, item):
        self.items.append(item)

    def sum_macro(self, macro):
        return sum([float(x.nutrition_json.get(macro)) for x in self.items])
    
    def calculate_all_macros(self):
        macros = ["calories", "fat", "saturated_fat",
                    "carbohydrates", "sugar", "fibre", "protein", "salt"]
        
        macro_percentages = []

        for macro in macros:
            try:
                ri = float(self.ri_data.get(f"ri_{macro}"))
                macro_sum = self.sum_macro(macro)
                perc = ( macro_sum / ri) * 100
            except Exception as e:
                print(e)
                print(macro)
            perc = str(perc) + "%"
            macro_percentages.append(f"{macro_sum}/{ri} ({perc})")

        return dict(zip(macros, macro_percentages))

class FoodItem:
    def __init__(self, nutrition_json):
        self.nutrition_json = nutrition_json
        self.load_data()

    def load_data(self):
        self.name = self.nutrition_json.get("name")
        self.url = self.nutrition_json.get("url")
        self.calories = self.nutrition_json.get("calories")
        self.fat = self.nutrition_json.get("fat")
        self.saturated_fat = self.nutrition_json.get("saturated_fat")
        self.carbohydrates = self.nutrition_json.get("carbohydrates")
        self.sugar = self.nutrition_json.get("sugar")
        self.fibre = self.nutrition_json.get("fibre")
        self.protein = self.nutrition_json.get("protein")
        self.salt = self.nutrition_json.get("salt")


def create_food_item():
    data_elements = ["name", "url", "calories", "fat", "saturated_fat",
                    "carbohydrates", "sugar", "fibre", "protein", "salt"]
    
    data = {}

    for element in data_elements:
        data[element] = input(f"{element}: ")
    
    filename = data["name"].replace(" ", "_") + ".json"

    with open(f"food/{filename}", "w") as f:
        f.write(json.dumps(data))

    return FoodItem(data)

