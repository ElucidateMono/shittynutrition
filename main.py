import sys
import os
import json
from food_data import FoodItem
from food_data import create_food_item
from food_data import Plan
from pprint import pprint

def get_item(name, items):
    for item in items:
        if name.lower() in item.name.lower():
            return item

def main():
    print("----Shitty Nutrition Calculator----")

    food_data = os.listdir("food")
    food_items = []
    #food_items.append(create_food_item())

    for item in food_data:
        with open(f"food/{item}", "r") as f:
            food_item = json.loads(f.read())
            food_item = FoodItem(food_item)
            food_items.append(food_item)
    
    print(f"Food items: {[x.name for x in food_items]}")
    
    running = True

    while running:
        cmd = input("> ")
        cmd, args = cmd.split(" ")[0], cmd.split(" ")[1:]

        if cmd == "info":
            item = food_items[int(args[0]) -1]
            pprint(item.nutrition_json)
        elif cmd == "add":
            food_items.append(create_food_item())
        elif cmd == "list":
            for idx, item in enumerate(food_items, start=1):
                print(f"{idx}) {item.name}")
        elif cmd == "calculate":
            plan = Plan("week")
            plan.add_item(get_item("Fusilli",food_items))
            plan.add_item(get_item("chicken",food_items))
            plan.add_item(get_item("chicken",food_items))
            plan.add_item(get_item("veg",food_items))
            plan.add_item(get_item("yoghurt",food_items))
            plan.add_item(get_item("yoghurt",food_items))

            print([x.name for x in plan.items])
            pprint(plan.calculate_all_macros())
        elif cmd == "exit":
            sys.exit(0)

if __name__ == "__main__":
    main()